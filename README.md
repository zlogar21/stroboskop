# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone git@bitbucket.org:zlogar21/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/zlogar21/stroboskop/commits/4c0db9af475c5ab04b1b26382e315cccf95a0534

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/zlogar21/stroboskop/commits/d72384c7ef208ed3fabc97e4759a9c37b759c487

Naloga 6.3.2:
https://bitbucket.org/zlogar21/stroboskop/commits/92537ace45db7c56b1118efa2dff49526db66248

Naloga 6.3.3:
https://bitbucket.org/zlogar21/stroboskop/commits/d0e91a78ba67a195555afb47ae41c2690610b60b

Naloga 6.3.4:
https://bitbucket.org/zlogar21/stroboskop/commits/fbe61186c9e497f6d4cfb0277494d0c3214b6711

Naloga 6.3.5:

```
git checkout master
git merge izgled
git push origin master
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/zlogar21/stroboskop/commits/a7f3e95bf794790fdf7bd108fdf85551fa4b00f1

Naloga 6.4.2:
https://bitbucket.org/zlogar21/stroboskop/commits/4a3553368a936a368cdeab4f4a8b71e4f7820d95

Naloga 6.4.3:
https://bitbucket.org/zlogar21/stroboskop/commits/e92313ffc0f0118a0ea31ada171c9d26d2bacb51

Naloga 6.4.4:
https://bitbucket.org/zlogar21/stroboskop/commits/b26d9533cf0dbc78fb0d6d045301d928859edfb7